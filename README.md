# Car Number Plate Detection Using Tesseract and OpenCV

#### With the growing need to automate checkpoints for security and organizing reasons, the use of car plates recognition becomes more popular these days.

#### In this project we use Tesseract and OpenCV to detect cars plates and then store the results in a CSV file to use it later for analytical and statistical purposes.

You can check [this video](https://youtu.be/AAPZLK41rek) to understand Tesseract library installation and usage:
______________________________________________________________
#### The following steps demonstrate the whole process:

## _1- Getting the original image_
![alt text](images/01.PNG)

## _2- Converting the image to grayscale_
![alt text](images/02.PNG)

## _3- Blurring grayscale image with bilateral filter to reduce the unwanted edges_
![alt text](images/03.PNG)

## _4- Extracting edges with canny filter_
![alt text](images/04.PNG)

## _5- Detecting the plate and surrounding it with a quadrilateral_
![alt text](images/05.PNG)

## _6- Masking the image to remove all unwanted data_
![alt text](images/06.PNG)

## _7- Reading the plate's text by using Tesseract OCR_
![alt text](images/08.PNG)

## _8- Reading each character individually by detecting its contour_
![alt text](images/10.PNG)

## _9- Storing the Datetime, Tesseract's detection result, and the manual detection in a CSV file_

________________________________________________________

## The Team Members:

* [Moaaz Al Shurbaji](https://www.linkedin.com/in/moaaz-al-shurbaji/)
* [Mohamad Shafiee Jaddini](https://www.linkedin.com/in/shafiee-jaddini/)
